<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();

        $roles = [
            ['name' => 'Admin', 'slug' => 'admin', 'description' => 'Allows all functionality'],
            ['name' => 'Guest', 'slug' => 'guest', 'description' => 'Allows only view home page'],
            ['name' => 'Manager', 'slug' => 'manager', 'description' => 'Allows to manage users']
        ];

        DB::table('roles')->insert($roles);
    }
}
