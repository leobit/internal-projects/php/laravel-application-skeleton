<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->delete();

        $permissions = [
            [
                'name' => 'View Users',
                'slug' => 'users.view',
                'description' => 'Allows to view users'
            ],
            [
                'name' => 'Create Users',
                'slug' => 'users.create',
                'description' => 'Allows to create users'
            ],
            [
                'name' => 'Edit Users',
                'slug' => 'users.edit',
                'description' => 'Allows to edit users'
            ],
            [
                'name' => 'Delete Users',
                'slug' => 'users.delete',
                'description' => 'Allows to delete users'
            ],
            [
                'name' => 'View Roles',
                'slug' => 'roles.view',
                'description' => 'Allows to view roles'
            ],
            [
                'name' => 'Create Roles',
                'slug' => 'roles.create',
                'description' => 'Allows to create roles'
            ],
            [
                'name' => 'Edit Roles',
                'slug' => 'roles.edit',
                'description' => 'Allows to edit roles'
            ],
            [
                'name' => 'Delete Roles',
                'slug' => 'roles.delete',
                'description' => 'Allows to delete roles'
            ],
            [
                'name' => 'View Permissions',
                'slug' => 'permissions.view',
                'description' => 'Allows to view permissions'
            ],
            [
                'name' => 'Create Permission',
                'slug' => 'permissions.create',
                'description' => 'Allows to create permission'
            ],
            [
                'name' => 'Edit Permission',
                'slug' => 'permissions.edit',
                'description' => 'Allows to edit permissions'
            ],
            [
                'name' => 'Delete Permissions',
                'slug' => 'permissions.delete',
                'description' => 'Allows to delete permissions'
            ],

        ];

        DB::table('permissions')->insert($permissions);
    }
}
