<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        $users = [
            [
                'name' => 'Oleg Bryn',
                'email' => 'obryn' .'@leobit.com',
                'password' => Hash::make('password'),
                'role_id' => 1
            ],
            [
                'name' => 'Oleksandr Guest',
                'email' => 'guest' .'@leobit.com',
                'password' => Hash::make('password'),
                'role_id' => 2
            ],
            [
                'name' => 'Olga Manager',
                'email' => 'manager' .'@leobit.com',
                'password' => Hash::make('password'),
                'role_id' => 3
            ],
        ];

        DB::table('users')->insert($users);
    }
}
