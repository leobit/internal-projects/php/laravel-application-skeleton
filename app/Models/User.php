<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;

/**
 * Class User
 *
 * @mixin Builder
 */
class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Find specified user by filled credentials
     *
     * @param $credentials
     * @return User|null
     */
    public function getUserByCredentials($credentials): ?User
    {
        return $this->where('email', '=', $credentials['email'])->first();
    }

    /**
     * Relation to the user's role
     *
     * @return BelongsTo
     */
    public function role() : BelongsTo
    {
        return $this->belongsTo(Role::class);
    }

    /**
     * Check if user has specified role
     *
     * @param string $slug
     * @return bool
     */
    public function hasRole(string $slug): bool
    {
        return $this->role()->where('slug', '=', $slug)->get()->isNotEmpty();
    }

    /**
     * Check if user role is admin
     *
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->hasRole('admin');
    }

    /**
     * Check if user has specified permission through role
     *
     * @param string $slug
     * @return bool
     */
    public function hasPermission(string $slug): bool
    {
        return $this->role()->first()->hasPermission($slug);
    }

    /**
     * Relation to the user's activity records
     *
     * @return HasMany
     */
    public function activity() : HasMany
    {
        return $this->hasMany(Activity::class);
    }

    /**
     * Check if user has any activity records
     *
     * @return bool
     */
    public function hasActivity() : bool
    {
        return $this->activity()->get()->isNotEmpty();
    }

    /**
     * Scope latest user activity collection.
     *
     * @param int $limit
     * @return Collection
     */

    public function scopeLatestActivity(int $limit = 5): Collection
    {
        return $this->activity()->latest()->limit($limit)->get();
    }

    /**
     * Check if user is blocked
     *
     * @return bool
     */
    public function isBlocked(): bool
    {
        return $this->activity()
            ->where('status', '=', 'blocked')
            ->whereDate('created_at', '>', Carbon::now()->subDays())
            ->get()
            ->isNotEmpty();
    }

    /**
     * Relation to the user's profile
     *
     * @return hasOne
     */
    public function profile(): HasOne
    {
        return $this->hasOne(Profile::class);
    }

    /**
     * Check if user has profile
     *
     * @return bool
     */
    public function hasProfile(): bool
    {
        return $this->profile()->get()->isNotEmpty();
    }
}
