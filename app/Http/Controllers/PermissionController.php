<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('can:view,App\Models\Permission')->only('view');
        $this->middleware('can:create,App\Models\Permission')->only('create','store');
        $this->middleware('can:edit,permission')->only('edit','update');
        $this->middleware('can:delete,permission')->only('delete','destroy');
    }

    /**
     * Display a listing of permissions
     */
    public function index()
    {
        return view('admin.permissions.index', [
            'permissions' => Permission::all()
        ]);
    }

    /**
     * Show the form for creating a new permission
     */
    public function create(Permission $permission)
    {
        return view('admin.permissions.create', [
            'permission' => $permission,
        ]);
    }

    /**
     * Store a newly created permission in storage.
     */
    public function store(Request $request, Permission $permission)
    {
        $permission->fill($request->only(['name', 'slug', 'description']));

        $permission->save();

        return redirect()->route('permissions.index');
    }

    /**
     * Show the form for editing the specified permission.
     */
    public function edit(Permission $permission)
    {
        return view('admin.permissions.edit', [
            'permission' => $permission,
        ]);
    }

    /**
     * Update the specified permission in storage.
     */
    public function update(Request $request, Permission $permission)
    {
        $permission->update(($request->only(['name', 'slug', 'description'])));

        return redirect()->route('permissions.index');
    }

    /**
     * Show the form for deleting the specified permission.
     */
    public function delete(Permission $permission)
    {
        return view('admin.permissions.delete', [
            'permission' => $permission,
        ]);
    }

    /**
     * Remove the specified permission from storage.
     */
    public function destroy(Permission $permission)
    {
        $permission->delete();

        return redirect()->route('permissions.index');
    }
}
