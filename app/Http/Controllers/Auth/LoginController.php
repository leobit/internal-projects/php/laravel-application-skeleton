<?php

namespace App\Http\Controllers\Auth;

use App\Services\UserActivity;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers, ThrottlesLogins;


    /**
     * Lockout Login Attempts static values
     *
     * @const int
     */
    const MAX_ATTEMPTS = 3;
    const DECAY_MINUTES = 1440;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request, UserActivity $userActivity)
    {
        $this->validateLogin($request);

        $user = $userActivity->getUser($this->credentials($request));

        if ($user && $user->hasActivity() && $user->isBlocked()) {
            $this->sendLockoutResponse($request);
        }

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            $userActivity->createActivity($request, 'blocked');
            $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            $userActivity->createActivity($request, 'allowed');
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form
        $this->incrementLoginAttempts($request);
        $userActivity->createActivity($request, 'failed');

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Generate lockout response message
     *
     * @return void
     * @throws ValidationException
     */
    protected function sendLockoutResponse(Request $request)
    {
        $seconds = $this->limiter()->availableIn(
            $this->throttleKey($request)
        );

        throw ValidationException::withMessages([
            $this->username() => [trans('auth.throttle', [
                'seconds' => $seconds,
                'minutes' => ceil($seconds / 60),
                'hours' => ceil($seconds / 3600)
            ])],
        ])->status(Response::HTTP_TOO_MANY_REQUESTS);
    }



    /**
     * Get the maximum number of login attempts to allow.
     *
     * @return int
     */
    public function maxAttempts(): int
    {
        return property_exists($this, 'maxAttempts') ? $this->maxAttempts : self::MAX_ATTEMPTS;
    }

    /**
     * Get the number of minutes to throttle for.
     *
     * @return int
     */
    public function decayMinutes(): int
    {
        return property_exists($this, 'decayMinutes') ? $this->decayMinutes : self::DECAY_MINUTES;
    }
}
