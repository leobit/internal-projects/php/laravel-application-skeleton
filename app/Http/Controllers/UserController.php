<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('can:view,App\Models\User')->only('index');
        $this->middleware('can:create,App\Models\User')->only('create','store');
        $this->middleware('can:edit,user')->only('edit','update');
        $this->middleware('can:delete,user')->only('delete','destroy');
    }

    /**
     * Display a listing of the users.
     */
    public function index()
    {
        return view('admin.users.index', [
            'users' => User::all(),
        ]);
    }

    /**
     * Show the form for creating a new user.
     */
    public function create(User $user)
    {
        return view('admin.users.create', [
            'user' => $user,
            'roles' => Role::all()->pluck('name', 'id')
        ]);
    }

    /**
     * Store a newly created user in storage.
     *
     */
    public function store(Request $request, User $user)
    {
        $request->merge([
            'password' => Hash::make($request->input('password'))
        ]);

        $user->fill($request->only(['name', 'email', 'password', 'role_id']));

        $user->save();

        return redirect()->route('users.index');
    }

    /**
     * Show the form for editing the specified user.
     *
     */
    public function edit(User $user)
    {
        return view('admin.users.edit', [
            'user' => $user,
            'roles' => Role::all()->pluck('name', 'id')
        ]);
    }

    /**
     * Update the specified user in storage.
     *
     */
    public function update(Request $request, User $user)
    {
        if ($request->input('password')) {
            $request->merge([
                'password' => Hash::make($request->input('password'))
            ]);
        }
        $user->update(array_filter($request->only(['name', 'email', 'password', 'role_id'])));

        return redirect()->route('users.index');
    }

    /**
     * Show confirmation form to delete specified user.
     */
    public function delete(User $user)
    {
        return view('admin.users.delete', [
            'user' => $user,
        ]);
    }

    /**
     * Remove the specified user from storage.
     *
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('users.index');
    }

    /**
     * Show activity status and the form to edit it
     */
    public function activity(User $user)
    {
        return view('admin.users.activity', [
            'user' => $user,
        ]);
    }
}
