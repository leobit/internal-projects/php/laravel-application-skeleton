<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\Permission;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('can:view,App\Models\Role')->only('index');
        $this->middleware('can:create,App\Models\Role')->only('create','store');
        $this->middleware('can:edit,role')->only('edit','update');
        $this->middleware('can:delete,role')->only('delete','destroy');
    }

    /**
     * Display a listing of roles
     */
    public function index()
    {
        return view('admin.roles.index', [
            'roles' => Role::all(),
        ]);
    }

    /**
     * Show the form for creating a new role
     */
    public function create(Role $role)
    {
        return view('admin.roles.create', [
            'role' => $role,
            'permissions' => Permission::all()
        ]);
    }

    /**
     * Store a newly created role in storage.
     */
    public function store(Request $request, Role $role)
    {
        $role->fill($request->only(['name', 'slug', 'description']));

        $role->save();

        $role->permissions()->sync($request->input(['permissions']));

        return redirect()->route('roles.index');
    }

    /**
     * Show the form for editing the specified role
     */
    public function edit(Role $role)
    {
        return view('admin.roles.edit', [
            'role' => $role,
            'permissions' => Permission::all()
        ]);
    }

    /**
     * Update the specified role in storage.
     */
    public function update(Request $request, Role $role)
    {
        $role->update(($request->only(['name', 'slug', 'description'])));

        $role->permissions()->sync($request->input(['permissions']));

        return redirect()->route('roles.index');
    }

    /**
     * Show the form for deleting the specified permission.
     */
    public function delete(Role $role)
    {
        return view('admin.roles.delete', [
            'role' => $role,
        ]);
    }

    /**
     * Remove the specified role from storage.
     */
    public function destroy(Role $role)
    {
        $role->delete();

        return redirect()->route('roles.index');
    }
}
