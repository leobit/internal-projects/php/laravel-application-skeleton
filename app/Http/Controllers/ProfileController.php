<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use App\Jobs\ProcessImageUpload;
use App\Models\User;
use App\Services\ImageProcessor;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show user profile.
     *
     */
    public function index()
    {
        $user = User::find(auth()->id());

        $profile = null;

        if ($user->hasProfile()) {
            $profile = $user->profile()->first();

            $image = $profile['image'];

            $imageConverted = ImageProcessor::getConvertedImage($image, '320x320');

            if (ImageProcessor::hasImage($imageConverted)) {
                $profile['image'] = $imageConverted;
            }
        }

        return view('admin.profile.index', [
            'user' => $user,
            'profile' => $profile
        ]);
    }

    /**
     * Show the form for editing profile.
     *
     */
    public function edit()
    {
        return view('admin.profile.create');
    }

    /**
     * Update or create new profile in storage.
     *
     */
    public function update(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg|max:10240'
        ]);

        $userId = auth()->id();

        $filePath = $request->file('image')->store('/', ['disk' => 'uploads']);

        $profile = Profile::updateOrCreate(['user_id' => $userId], ['image' => $filePath]);

        ProcessImageUpload::dispatch($profile);

        return redirect()->route('profile')->with(['status' => 'Profile updated successfully.']);
    }
}
