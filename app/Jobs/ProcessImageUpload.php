<?php

namespace App\Jobs;

use App\Models\Profile;
use App\Services\ImageProcessor;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessImageUpload implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The Profile and image instance.
     *
     * @var Profile
     * @var $image
     */
    protected $profile;
    protected $image;

    /**
     * Create a new job instance.
     *
     * @param Profile $profile
     * @return void
     */
    public function __construct(Profile $profile)
    {
        $this->image = $profile['image'];
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws FileNotFoundException
     */
    public function handle(ImageProcessor $imageProcessor)
    {
        $imageProcessor->updateImage($this->image);
    }
}
