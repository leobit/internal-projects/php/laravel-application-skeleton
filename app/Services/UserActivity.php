<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Http\Request;

class UserActivity
{
    protected $users;

    /**
     * Create a new user instance
     *
     * @param User $users
     */
    public function __construct(User $users)
    {
        $this->users = $users;
    }

    /**
     * Get specified user by credentials
     *
     * @param $credentials
     * @return User|null
     */
    public function getUser($credentials): ?User
    {
        return $this->users->getUserByCredentials($credentials);
    }

    /**
     * Create a new activity record
     *
     * @param Request $request
     * @param $status
     */
    public function createActivity(Request $request, $status)
    {   $user = $this->getUser($request);
        if ($user) {
            $user->activity()->create([
                'status' => $status,
                'ip' => $request->ip(),
                'created_at' => now()
            ]);
        }
    }
}
