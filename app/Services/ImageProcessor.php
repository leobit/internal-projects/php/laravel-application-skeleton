<?php

namespace App\Services;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ImageProcessor
{

    /**
     * Process image and store with resolution prefix
     *
     * @param  $image
     * @return void
     * @throws FileNotFoundException
     */
    public function updateImage($image)
    {
        $imageFile = Image::make(self::getImageFile($image));

        $watermark = asset('images/lion.png');

        $imageFile->fit(320)
            ->insert($watermark, 'bottom-right', 10, 10)
            ->save(storage_path('app/public/uploads/320x320_' . $image));
    }

    /**
     * Check if image exists in storage
     *
     * @param  $image
     * @return bool
     */
    public static function hasImage($image): bool
    {
        return Storage::disk('uploads')->exists($image);
    }

    /**
     * Get image file from storage
     *
     * @param  $image
     * @return string
     * @throws FileNotFoundException
     */
    public static function getImageFile($image): string
    {
        return Storage::disk('uploads')->get($image);
    }

    /**
     * Get image name with specified resolution
     *
     * @param  $image
     * @param  $resolution
     * @return string
     */
    public static function getConvertedImage($image, $resolution): string
    {
        return $resolution . '_' . $image;
    }
}
