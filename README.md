## Laravel Application Skeleton

Custom project prototype based on Laravel Framework

## Installation

- clone project
- move to project root directory
   `cd /path_to_project_root`
- install php packages & dependencies 
   `composer install`
- create environment .env file from example 
   `copy .env.example .env`
- generate application key (optional)
   `php artisan key:generate`
- set environment variables in .env file
   `nano .env`
- run database migration and database seeding
   `php artisan migrate --seed`
- install npm packages 
   `npm install`
- compile static assets 
   `npm run dev` or 
   `npm run production`
   

