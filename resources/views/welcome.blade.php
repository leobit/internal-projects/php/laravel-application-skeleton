@extends('layouts/app')

@section('content')
    <div class="container box-container">
        <div class="row justify-content-center">
            <h3><strong>Welcome to Laravel Application Skeleton. To see features, please, log in.</strong></h3>
            <p><strong>Email: sample@mail.com, Password: password</strong></p>
        </div>
        <div class="row justify-content-center">
            <a href="{{ route('login') }}" class="btn btn-action btn-primary">{{ __('Login') }}</a>
        </div>
    </div>
@endsection
