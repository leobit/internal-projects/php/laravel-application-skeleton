<header class="header">
    <div class="container">
        <div class="header-inner">
            <img class="header-logo-img" width="41" height="41" src="{{ asset('images/lion.svg') }}" alt="Leobit" title="Leobit">
            <span class="header-logo-text">{{ config('app.name', 'Laravel Application Skeleton') }}</span>
        </div>
        @auth
            @include('layouts/navbar')
        @endauth
    </div>
</header>
