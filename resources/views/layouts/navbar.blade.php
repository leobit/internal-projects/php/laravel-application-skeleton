<nav class="navbar" role="navigation" aria-label="main navigation">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="/home">{{ __('Home') }}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="https://leobit.com">{{ __('Website') }}</a>
        </li>
        @canany(['view','create', 'edit', 'delete'], App\Models\User::class)
            <li class="nav-item">
                <a class="nav-link" href="/users">{{ __('Manage Users') }}</a>
            </li>
        @endcanany
        @canany(['view','create', 'edit', 'delete'], App\Models\Role::class)
                <li class="nav-item">
                    <a class="nav-link" href="/roles">{{ __('Manage Roles') }}</a>
                </li>
        @endcanany
        @canany(['view','create', 'edit', 'delete'], App\Models\Permission::class)
                <li class="nav-item">
                    <a class="nav-link" href="/permissions">{{ __('Manage Permissions') }}</a>
                </li>
        @endcanany
    </ul>
    <div class="nav-profile">
        <a href="{{ route('profile') }}">
            <img src="{{ asset('images/profile.svg')}}" width="50" height="50">
        </a>
        <div class="nav-btn">
            <a href="{{ route('logout') }}" class="btn btn-action btn-danger">{{ __('Logout') }}</a>
        </div>
    </div>
</nav>
