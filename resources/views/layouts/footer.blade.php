<footer class="footer">
    <div class="container">
        <div class="copyright">
            <span>All Rights Reserved. Leobit, LLC @php echo date('Y'); @endphp. Created by Oleg Bryn</span>
        </div>
    </div>
</footer>
