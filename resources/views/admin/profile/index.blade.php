@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="box-container">
            <div class="row head justify-content-center">
                <h1>{{ __('Profile') }}</h1>
            </div>
            <div class="profile">
                <div class="col">
                    <div class="row profile-name justify-content-center">
                        <h3><strong>{{ $user->name }}</strong></h3>
                    </div>
                    <div class="row justify-content-center">
                        <div class="profile-image">
                            @if ( $profile && $profile['image'])
                                <img src="{{ asset(('storage/uploads/' . $profile['image'])) }}" alt="image">
                            @else
                                <img src="{{ asset('images/default_profile.jpg') }}" alt="image">
                            @endif
                        </div>
                    </div>
                    <div class="row justify-content-center profile-buttons">
                        <a href="{!! route('users.edit', $user) !!}" class="btn btn-action">{{ __('Edit Account') }}</a>
                        <a href="{!! route('profile.edit') !!}" class="btn btn-action">{{ __('Edit Profile') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
