@extends('layouts.app')

@section('content')
    <div class="box-container container">
        <h1>Create Profile</h1>
        {{ Form::open(['route' => 'profile.update', 'files' => true]) }}

        <div class="form-group">
            {!! Form::label('image', 'Image') !!}
            {!! Form::file('image', ['class' => 'form-control-file', 'required']) !!}
        </div>
        {!! Form::submit('Create', ['class' => 'btn btn-success']) !!}
        {!! link_to_route('home', 'Back', [], ['class' => 'btn btn-secondary']) !!}
        {!! Form::close() !!}
    </div>
@endsection
