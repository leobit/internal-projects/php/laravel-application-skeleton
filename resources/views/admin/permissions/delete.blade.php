@extends('layouts.app')

@section('content')
    <div class="box-container container">
        {{ Form::model($permission, ['method' =>'DELETE', 'route' => ['permissions.destroy', $permission->id]]) }}
        @csrf
        <h3><small class="text-muted">Are you sure to delete permission?</small></h3>
        <h1> {{ Form::label('name', $permission->name) }}</h1>
        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
        {!! link_to_route('permissions.index', 'Back', [], ['class' => 'btn btn-secondary']) !!}
        {!! Form::close() !!}
    </div>
@endsection
