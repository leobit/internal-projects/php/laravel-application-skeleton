@extends('layouts.app')

@section('content')
    <div class="box-container container">
        <h1>{{ __('Add Permission') }}</h1>
        {{ Form::model($permission, ['method' =>'POST', 'route' => ['permissions.store', $permission]]) }}
        @csrf
        <div class="form-group">
            {!! Form::label('name', 'Name') !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter permission name', 'required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('slug', 'Slug') !!}
            {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'Enter permission slug', 'required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('description', 'Description') !!}
            {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Enter description', 'required']) !!}
        </div>
        {!! Form::submit('Add', ['class' => 'btn btn-success']) !!}
        {!! link_to_route('permissions.index', 'Back', [], ['class' => 'btn btn-secondary']) !!}
        {!! Form::close() !!}
    </div>
@endsection
