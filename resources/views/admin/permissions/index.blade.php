@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="box-container">
            <div class="row head justify-content-center align-items-center">
                <h1>{{ __('Permissions') }}</h1>
                <div class="head-btn">
                    <a href="{!! route('permissions.create') !!}" class="btn btn-action btn-primary">{{ __('Add') }}</a>
                </div>
            </div>
            <div class="list">
                <div class="row item head align-items-baseline justify-content-center">
                    <div class="col-sm-3 field"><strong>{{ __('Name') }}</strong></div>
                    <div class="col-sm-3 field"><strong>{{ __('Slug') }}</strong></div>
                    <div class="col-sm-4 field"><strong>{{ __('Description') }}</strong></div>
                    <div class="col-sm-2 field"><strong>{{ __('Actions') }}</strong></div>
                </div>
                @foreach($permissions as $item)
                    <div class="row item align-items-baseline justify-content-center">
                        <div class="col-sm-3 field">{{ $item->name }}</div>
                        <div class="col-sm-3 field">{{ $item->slug }}</div>
                        <div class="col-sm-4 field">{{ $item->description }}</div>
                        <div class="col-sm-2">
                            <a href="{!! route('permissions.edit', $item) !!}" class="btn btn-action btn-primary">Edit</a>
                            <a href="{!! route('permissions.delete', $item) !!}" class="btn btn-action btn-danger">Delete</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
