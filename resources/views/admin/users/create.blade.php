@extends('layouts.app')

@section('content')
    <div class="box-container container">
        <h1>Create User</h1>
        {{ Form::model($user, ['method' =>'POST', 'route' => ['users.store', $user]]) }}
        @csrf
        <div class="form-group">
            {!! Form::label('name', 'User Name') !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter user name', 'required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('email', 'Email') !!}
            {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Enter user email', 'required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('password', 'Password') !!}
            {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Enter user password', 'required']) !!}
        </div>
        @if($roles)
        <div class="form-group">
            {!! Form::label('role', 'Role') !!}
            {!! Form::select('role_id', $roles, null, ['class' => 'form-control', 'required']); !!}
        </div>
        @endif
        {!! Form::submit('Create', ['class' => 'btn btn-success']) !!}
        {!! link_to_route('users.index', 'Back', [], ['class' => 'btn btn-secondary']) !!}
        {!! Form::close() !!}
    </div>
@endsection
