@extends('layouts.app')

@section('content')
    <div class="box-container container">
        <h1>Edit User</h1>
        {{ Form::model($user, ['method' =>'PUT', 'route' => ['users.update', $user->id]]) }}
        @csrf
        <div class="form-group">
            {!! Form::label('name', 'User Name') !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter new name']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('email', 'Email') !!}
            {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Enter new email']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('password', 'Password') !!}
            {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Enter new password']) !!}
        </div>
        @if($roles)
            <div class="form-group">
                {!! Form::label('role', 'Role') !!}
                {!! Form::select('role_id', $roles, $user->role->id, ['class' => 'form-control', 'required']); !!}
            </div>
        @endif
        {!! Form::submit('Update', ['class' => 'btn btn-success']) !!}
        {!! link_to_route('users.index', 'Back', [], ['class' => 'btn btn-secondary']) !!}
        {!! Form::close() !!}
    </div>
@endsection
