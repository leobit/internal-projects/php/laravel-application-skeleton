@extends('layouts.app')

@section('content')
    <div class="box-container container">
        {{ Form::model($user, ['method' =>'DELETE', 'route' => ['users.destroy', $user->id]]) }}
        @csrf
        <h3><small class="text-muted">Are you sure to delete user?</small></h3>
        <h1> {{ Form::label('name', $user->name) }}</h1>
        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
        {!! link_to_route('users.index', 'Back', [], ['class' => 'btn btn-secondary']) !!}
        {!! Form::close() !!}
    </div>
@endsection
