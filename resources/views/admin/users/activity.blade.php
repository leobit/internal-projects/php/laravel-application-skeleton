@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="box-container">
            <div class="row head">
                <h1>{{ __('Activity') }}</h1>
            </div>
            <div class="row justify-content-center">
                <h3>{{ $user->name }}</h3>
            </div>
            @if ($user->hasActivity())
                <div class="list box-container">
                    <div class="row item align-items-baseline justify-content-center">
                        <div class="col-sm-4 field text-center"><strong>{{ __('Status') }}</strong></div>
                        <div class="col-sm-4 field text-center"><strong>{{ __('IP') }}</strong></div>
                        <div class="col-sm-4 field text-center"><strong>{{ __('Created at') }}</strong></div>
                    </div>
                    @foreach($user->scopeLatestActivity('5') as $activity)
                        <div class="row item align-items-baseline justify-content-center">
                            <div class="col-sm-4 field text-center text-uppercase">{{ $activity->status }}</div>
                            <div class="col-sm-4 field text-center">{{ $activity->ip }}</div>
                            <div class="col-sm-4 field text-center">{{ $activity->created_at }}</div>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="row justify-content-center">
                    <p>{{ __('No activity founded') }}</p>
                </div>
            @endif
            <div class="row justify-content-center">
                {!! link_to_route('users.index', 'Back', [], ['class' => 'btn btn-secondary']) !!}
            </div>
        </div>
    </div>
@endsection
