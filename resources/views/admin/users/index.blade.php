@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="box-container">
            <div class="row head justify-content-center align-items-center">
                <h1>{{ __('Users') }}</h1>
                <div class="head-btn">
                    <a href="{!! route('users.create') !!}" class="btn btn-action btn-primary">{{ __('Create') }}</a>
                </div>
            </div>
            <div class="list">
                <div class="row item head align-items-baseline justify-content-center">
                    <div class="col-sm-2 field"><strong>{{ __('Name') }}</strong></div>
                    <div class="col-sm-2 field"><strong>{{ __('Email') }}</strong></div>
                    <div class="col-sm-2 field"><strong>{{ __('Role') }}</strong></div>
                    <div class="col-sm-3 field"><strong>{{ __('Created at') }}</strong></div>
                    <div class="col-sm-3 field"><strong>{{ __('Actions') }}</strong></div>
                </div>
                @foreach($users as $user)
                    <div class="row item align-items-baseline justify-content-center">
                        <div class="col-sm-2 field">{{ $user->name }}</div>
                        <div class="col-sm-2 field">{{ $user->email }}</div>
                        <div class="col-sm-2 field">{{ $user->role->name }}</div>
                        <div class="col-sm-3 field">{{ $user->created_at }}</div>
                        <div class="col-sm-3">
                            <a href="{!! route('users.activity', $user) !!}" class="btn btn-action btn-info">Activity</a>
                            <a href="{!! route('users.edit', $user) !!}" class="btn btn-action btn-primary">Edit</a>
                            <a href="{!! route('users.delete', $user) !!}" class="btn btn-action btn-danger">Delete</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
