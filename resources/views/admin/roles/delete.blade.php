@extends('layouts.app')

@section('content')
    <div class="box-container container">
        {{ Form::model($role, ['method' =>'DELETE', 'route' => ['roles.destroy', $role->id]]) }}
        @csrf
        <h3><small class="text-muted">Are you sure to delete role?</small></h3>
        <h1> {{ Form::label('name', $role->name) }}</h1>
        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
        {!! link_to_route('roles.index', 'Back', [], ['class' => 'btn btn-secondary']) !!}
        {!! Form::close() !!}
    </div>
@endsection
