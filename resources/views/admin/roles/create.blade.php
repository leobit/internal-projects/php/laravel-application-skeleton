@extends('layouts.app')

@section('content')
    <div class="box-container container">
        <h1>{{ __('Add Role') }}</h1>
        {{ Form::model($role, ['method' =>'POST', 'route' => ['roles.store', $role]]) }}
        @csrf
        <div class="form-group">
            {!! Form::label('name', 'Name') !!}
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Enter role name', 'required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('slug', 'Slug') !!}
            {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'Enter role slug', 'required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('description', 'Description') !!}
            {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Enter description', 'required']) !!}
        </div>
        @if($permissions)
            <div class="form-group">
                {!! Form::label('permissions', 'Permissions') !!}
                @foreach($permissions as $permission)
                    <div class="form-check">
                        {!! Form::checkbox("permissions[$permission->id]", $permission->id, ['class' => 'form-check-input']); !!}
                        {!! Form::label("permissions[$permission->id]", $permission->name, ['class' => 'form-check-label']) !!}
                    </div>
                @endforeach
            </div>
        @endif
        {!! Form::submit('Add', ['class' => 'btn btn-success']) !!}
        {!! link_to_route('roles.index', 'Back', [], ['class' => 'btn btn-secondary']) !!}
        {!! Form::close() !!}
    </div>
@endsection
