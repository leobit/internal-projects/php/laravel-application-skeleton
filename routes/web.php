<?php


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RoleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 *
 */
Route::redirect('/', '/welcome');
Route::view('/welcome', 'welcome');
Route::view('/home', 'home')->middleware('auth')->name('home');

/*
 * User CRUD routes group
 */
Route::resource('users', UserController::class)->except('show');
Route::get('users/{user}/delete', [UserController::class, 'delete'])->name('users.delete');
Route::get('users/{user}/activity', [UserController::class, 'activity'])->name('users.activity');

/*
 * Uses profile routing group
 */
Route::get('profile', [ProfileController::class, 'index'])->name('profile');
Route::get('profile/edit', [ProfileController::class, 'edit'])->name('profile.edit');
Route::post('profile', [ProfileController::class, 'update'])->name('profile.update');

/*
 * Role CRUD routes group
 */
Route::resource('roles', RoleController::class)->except('show');
Route::get('roles/{role}/delete', [RoleController::class, 'delete'])->name('roles.delete');

/*
 * Permission CRUD routes group
 */
Route::resource('permissions', PermissionController::class)->except('show');
Route::get('permissions/{permission}/delete', [PermissionController::class, 'delete'])
    ->name('permissions.delete');

/*
 * Auth routes group
 */
Route::get('login', [App\Http\Controllers\Auth\LoginController::class, 'showLoginForm'])->name('login');
Route::post('login', [App\Http\Controllers\Auth\LoginController::class, 'login']);
Route::get('logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');

