<?php

namespace Tests\Feature\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Tests\TestCase;

class LoginControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example
     * @test
     *
     * @return void
     */
    public function basicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(302);
    }

    /**
     * Login displays login form
     * @test
     *
     * @return void
     */
    public function loginDisplaysTheLoginForm()
    {
        $response = $this->get(route('login'));

        $response->assertStatus(200);
        $response->assertViewIs('auth.login');
    }

    /**
     * Login displays validation errors
     * @test
     *
     * @return void
     */
    public function loginDisplaysValidationErrors()
    {
        $response = $this->post('/login', []);

        $response->assertStatus(302);
        $response->assertSessionHasErrors('email');
    }

    /**
     * Login authenticates and redirects user
     * @test
     *
     * @return void
     */
    public function loginAuthenticatesAndRedirectsUser()
    {
        $user = User::factory()->create();

        $response = $this->post(route('login'), [
            'email' => $user->email,
            'password' => 'password'
        ]);

        $response->assertRedirect(route('home'));
        $this->assertAuthenticatedAs($user);

    }
}
